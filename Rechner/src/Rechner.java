import java.util.Scanner; // Import der Klasse Scanner 
 
public class Rechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
    
        
    //Operator wird abgefragt.
    System.out.print("Bitte geben sie einen Operator ein (+,-,*,/)");
    char operato = myScanner.next().charAt(0);
    
    System.out.print("Bitte geben Sie eine Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    double zahl1 = myScanner.nextDouble();  
     
    System.out.print("Bitte geben Sie eine zweite Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    double zahl2 = myScanner.nextDouble();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    
    double ergebnis = 0.0;
    
    if (operato == '+') {
     ergebnis = (zahl1) + (zahl2);  
    }
    if (operato == '-') {
        ergebnis = (zahl1) - (zahl2);  
    } 
    if (operato == '*') {
        ergebnis = (zahl1) * (zahl2);  
       }
    if (operato == '/') {
        ergebnis = (zahl1) / (zahl2);  
       }
    

    System.out.print("\n\n\nErgebnis der Rechnung lautet: "); 
    System.out.print(ergebnis);
    
    myScanner.close(); 
     
  }    
}