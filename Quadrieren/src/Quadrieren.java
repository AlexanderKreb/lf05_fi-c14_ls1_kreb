public class Quadrieren {
   
	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
		double x = 5;
				
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis= quadriere(x);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
	}	
	
	
	static double quadriere(double x) {
		
		double ergebnis = x * x ;
		return ergebnis;
		
	}
}
