import java.util.Scanner;
import java.lang.Math.*;

public class AB_Methoden_I {
	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		aufgabe1();
		aufgabe2();
		aufgabe4();
	}

	public static void aufgabe1() {											//Beginn Aufgabe 1
		
		System.out.println("Aufgabe1:\n");
		 
			  ausgabe(1, "Mana"); 
			  ausgabe(2, "Elise"); 
			  ausgabe(3, "Johanna"); 
			  ausgabe(4, "Felizitas"); 
			  ausgabe(5, "Karla"); 
			  System.out.println(vergleichen(1, 2)); 
			  System.out.println(vergleichen(1, 5)); 
			  System.out.println(vergleichen(3, 4));
			  System.out.println("Meine Erwartungen waren alle korrekt.\n"); //Augabenauswertung 1
			 } 
			 
			 public static void ausgabe(int zahl, String name) { 
			  System.out.println(zahl + ": " + name); 
			 } 
			 
			 public static boolean vergleichen(int arg1, int arg2) { 
			  return (arg1 + 8) < (arg2 * 3); 
			 		  
	}
			 public static void aufgabe2() {								//Beginn Aufgabe 2
				 
				 System.out.println("Aufgabe2:\n");
				 
				 double wert1 =2.36;
				 double wert2 =7.78;
				 double ergebnis= multiplikation(wert1,wert2);
				 
				 System.out.println(wert1 + "*" + wert2 + "=" + ergebnis);
				 System.out.println();
			 }
	
			 public static double multiplikation(double x, double y) {
				 
				 double ergebnis = x*y;
				 return ergebnis;
				 
			 }
	
		public static void aufgabe4() 	{										//Beginn Aufgabe 4 (3 existiert nicht)
			//variablen deklarieren
			
			//werte f�r variablen einlesen (scanner)
			
			//abfrage
			System.out.println("Bitte w�hlen sie Den K�rper,dessen Volumen sie berechnen m�chten (1-4)");
			System.out.println("1=W�rfel, 2=Quader, 3=Pyramide, 4=Kugel");
			int entscheidung = scanner.nextInt();
			
			if(entscheidung == 1) {
				volumenW�rfel();													//aufruf deiner methode1
			}
			else if (entscheidung == 2) {
				volumenQuader();																//aufruf deiner methode2
			}
			else if (entscheidung == 3) {
				volumenPyramide();													//aufruf deiner methode3
			}
			else if (entscheidung == 4) {
				volumenKugel();														//aufruf deiner methode4
			}
			else {
				System.out.println("Bitte nur einen Wert von 1-4 angeben!");
			}
		}
			
			public static void volumenW�rfel() {				//Methode Volumen W�rfel
				
				double a;
				
				System.out.println("Geben sie die Kantenl�nge des W�rfels an");
				
				a = scanner.nextDouble();
				
				double Vw�rfel = a*a*a;
				
				System.out.println("Das Volumen des W�rfels betr�gt " + Vw�rfel);
				
				}
			
			public static void volumenQuader() {				// Methode Volumen Quader
				
				double a;
				double b;
				double c;
				
				System.out.println("Geben Sie die Kantenl�nge a des Quaders an");
				a = scanner.nextDouble();
				
				System.out.println("Geben Sie die Kantenl�nge b des Quaders an");
				b = scanner.nextDouble();
				
				System.out.println("Geben Sie die Kantenl�nge c des Quaders an");
				c = scanner.nextDouble();
				
				double Vquader = a * b * c ;
				
				System.out.println("Das Volumen des Quaders betr�gt " + Vquader);
			
			}
			public static void volumenPyramide() {				// Methode Volumen Pyramide
				
				double a;
				double h;
				
				System.out.println("Geben Sie die Kantenl�nge a der Pyramide an");
				a = scanner.nextDouble();
				
				System.out.println("Geben Sie die H�he h der Pyramide an");
				h = scanner.nextDouble();
				
				double Vpyramide = a*a*h/3;
				
				System.out.println("Das Volumen der Pyramide betr�gt " + Vpyramide);
			}
			
			public static void volumenKugel() {						//Methode Volumen Kugel
				
				double r;
				
				System.out.println("Geben Sie den Radius der Kugel an");
				r = scanner.nextDouble();
				
				double Vkugel = 4/3 * Math.pow(r, 3.0) * Math.PI ;
				
				System.out.println("Das Volumen der Kugel betr�gt" + Vkugel);
			}	
			
			
			
			
			
			
	}

