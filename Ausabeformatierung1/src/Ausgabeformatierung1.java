
public class Ausgabeformatierung1 {				//abgabeaufgabe unten
	public static void main(String[] args) {
		String name = "Alex";
		System.out.print("Mein Name ist \"Alex\". \n"); //print = string in console, println= zeilenumbruch nach dem string
		System.out.print(""+name+" ist mein Name.\n");
		System.out.println("      *      ");
		System.out.println("     ***     ");
		System.out.println("    *****    ");
		System.out.println("   *******   ");
		System.out.println("  *********  ");
		System.out.println(" *********** ");
		System.out.println("*************");
		System.out.println("     ***     ");
		System.out.println("     ***     ");
												//Abgabeaufgabe folgend
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		System.out.printf( "|%.2f| \n" ,     a);
		System.out.printf( "|%.2f| \n" ,     b);
		System.out.printf( "|%.2f| \n" ,     c);
		System.out.printf( "|%.2f| \n" ,     d);
		System.out.printf( "|%.2f| \n" ,     e); 
													//oooder:
		System.out.printf( " |%.2f| \n |%.2f| \n |%.2f| \n |%.2f| \n |%.2f| \n" ,a ,b ,c ,d ,e);
	}

}
