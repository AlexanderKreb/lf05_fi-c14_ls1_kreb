

public class Einf�hrung_Methoden {

   public static void main(String[] args) {
	   
// (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // =========================== 	
	   
	   	double x = 2.0;
	   	double y = 4.0;
	   	double m = Verarbeitung(x,y);
	  
	   	System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
        
   }
   
	   public static double Verarbeitung(double x,double y) {
		// (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      double m = (x + y) / 2.0;   
		return m ;      
   }
	  
		   
	   
}
